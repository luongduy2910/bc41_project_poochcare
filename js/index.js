var darkValue = document.getElementById('dark') ; 
var lightValue = document.getElementById('light'); 
var headerValue = document.getElementById('header__nav');

function themeDark () {
    darkValue.style.display = "none" ; 
    lightValue.style.display = "block" ;
    headerValue.classList.add("background__dark") ;
    document.getElementById('about').classList.add('about__dark') ; 
    document.getElementById('choose').classList.add('choose__dark');
    document.getElementById('plans').classList.add('plans__dark') ; 
    document.getElementById('nutrition').classList.add('nutrition__dark'); 
    document.getElementById('testimonials').classList.add('testimonials__dark'); 
    document.getElementById('update').classList.add('update__dark') ; 
    
}

function themeLight () {
    darkValue.style.display = "block" ; 
    lightValue.style.display = "none" ;
    headerValue.classList.remove('background__dark');
    document.getElementById('about').classList.remove('about__dark') ; 
    document.getElementById('choose').classList.remove('choose__dark');
    document.getElementById('plans').classList.remove('plans__dark');
    document.getElementById('nutrition').classList.remove('nutrition__dark') ; 
    document.getElementById('testimonials').classList.remove('testimonials__dark'); 
    document.getElementById('update').classList.remove('update__dark') ; 

}